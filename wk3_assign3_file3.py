# Code to numerically calculate gradient of pressure using 2-
# and 3-point difference methods, and then
# use that in the geostrophic wind equation. 
# Plots to compare the two methods and analyze their errors.

import numpy as np
import matplotlib.pyplot as plt
from wk3_assign3_file2 import *
from wk3_assign3_file1 import *

def geostrophicWind():
    """Calculate the geostrophic wind analytically and numerically, then plot"""
    
   # Resolution and size of domain
    N = 10 # number of intervals to divide space into
    ymin = physProps['ymin']
    ymax = physProps['ymax']
    dy = (ymax-ymin)/N # length of spacing
    
    # spatial dimension, y:
    y = np.linspace(ymin,ymax,N+1)
    
    # exact geostrophic wind
    uExact = uGeoExact(y,physProps)
    
    # pressure at y points
    p = pressure(y,physProps)
    
    # pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    # pressure gradient and wind using three point differences
    dpdy = gradient_3point(p, dy)
    u_3point = geoWind(dpdy, physProps)
    
    # Graph to compare numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot approximate 2-point and exact wind at y points
    plt.figure(1)
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_2point, '*k-', label = 'Two-point differences', \
             ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent.pdf')
    plt.show()
    
    # Plot approximate 3-point and exact wind at y points
    plt.figure(2)
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_3point, '*k-', label = 'Three-point differences', \
             ms = 12, markeredgewidth = 1.5, markerfacecolor = 'none')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent3point.pdf')
    plt.show()
    
    # Plot only 2-point errors
    plt.figure(3)
    plt.plot(y/1000,abs(u_2point-uExact),label = '2 pt error')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('analytic-numerical (m/s)')
    plt.tight_layout()
    plt.savefig('geoWinderror2point.pdf')
    plt.show()
    
    # Plot errors for both 2-point and 3-point
    plt.figure(4)
    plt.plot(y/1000,abs(u_2point-uExact),label = '2 pt error')
    plt.plot(y/1000,abs(u_3point-uExact),label = '3 pt error')
    plt.legend(loc = 'best')
    plt.xlabel('y (km)')
    plt.ylabel('analytic-numerical (m/s)')
    plt.tight_layout()
    plt.savefig('geoWinderrorboth.pdf')
    plt.show()
    

if __name__ == "__main__":
    geostrophicWind()
    