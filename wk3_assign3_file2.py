# Functions for calculating gradients
# Testing numerical properties of the functions

import numpy as np
import matplotlib.pyplot as plt
from wk3_assign3_file1 import *

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 2-point differences. Returns array the same size as f"""
    
    dfdx = np.zeros_like(f)
    
    # Two point differences at end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[len(f)-1]-f[len(f)-2])/dx
    
    # Centered differences for the midpoints
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx

def test_gradient_2point():
    # importing data from file 1
    N = 10
    ymin = physProps['ymin']
    ymax = physProps['ymax']
    y = np.linspace(ymin,ymax,N+1)
    p = pressure(y,physProps)
    uExact = uGeoExact(y,physProps)

    # finding error at N = 10 resolution
    res_10 = (ymax - ymin)/N
    grad_10 = gradient_2point(p,res_10)
    wind_10 = geoWind(grad_10, physProps)
    error1 = abs(wind_10[5] - uExact[5])
    print('wind10_5',wind_10[5])
    print('error1',error1)

    # finding error at N = 20 resolution
    New_N = 20 
    new_y = np.linspace(ymin,ymax,New_N+1)
    new_p = pressure(new_y,physProps)
    new_uExact = uGeoExact(new_y,physProps)
    res_20 = (ymax - ymin)/New_N
    grad_20 = gradient_2point(new_p,res_20)
    wind_20 = geoWind(grad_20, physProps)
    error2 = abs(wind_20[10] - new_uExact[10])
    print('wind20_10',wind_20[10])
    print('error2',error2)

    # calculate order of accuracy using errors at two different resolutions
    order_acc = (np.log(error1)-np.log(error2))/(np.log(res_10)-np.log(res_20))
    print('order of accuracy for 2 point',order_acc)

    assert 1.999 < order_acc < 2.001, \
            "gradient_2point has the wrong order accuracy"
            
    # Create plot to estimate order of accuracy
    # big font
    font = {'size' : 14}
    plt.rc('font', **font)
    # data from above
    ress = [res_10,res_20]
    errs = [error1,error2]
    plt.plot(ress,errs)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('resolution')
    plt.ylabel('error')
    plt.grid(True,which='both')
    plt.tight_layout()
    plt.savefig('Orderacc_2point.pdf')
    plt.show()
      
test_gradient_2point()

def gradient_3point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance
    dx apart using 3-point differences. Returns array the same size as f"""
    
    dfdx = np.zeros_like(f)
    
    # Three point differences at first, second, second to last, and last points
    dfdx[0] = (-f[2] + 4*f[1] - 3*f[0])/(2*dx)
    
    dfdx[1] = (-f[3] + 4*f[2] - 3*f[1])/(2*dx)
    
    dfdx[-1] = (f[len(f)-3] - 4*f[len(f)-2] + 3*f[len(f)-1])/(2*dx)


    dfdx[-2] = (f[len(f)-4] - 4*f[len(f)-3] + 3*f[len(f)-2])/(2*dx)
    
    # Three point differences for midpoints
    for i in range(1,len(f)-2): # from 3rd to 3rd to last points
        #dfdx[i] = (-f[i+2] + 4*f[i+1] - 3*f[i])/(2*dx)
        dfdx[i] = (f[i+2] - 4*f[i-1] + 3*f[i])/(6*dx)
    return dfdx

def test_gradient_3point():
    # importing data from file 1
    N = 10
    ymin = physProps['ymin']
    ymax = physProps['ymax']
    y = np.linspace(ymin,ymax,N+1)
    p = pressure(y,physProps)
    uExact = uGeoExact(y,physProps)

    # finding error at N = 10 resolution
    res_10 = (ymax - ymin)/N
    grad_10 = gradient_3point(p,res_10)
    wind_10 = geoWind(grad_10, physProps)
    error1 = abs(wind_10[5] - uExact[5])
    print('error1',error1)

    # finding error at N = 20 resolution
    New_N = 20 
    new_y = np.linspace(ymin,ymax,New_N+1)
    new_p = pressure(new_y,physProps)
    new_uExact = uGeoExact(new_y,physProps)
    res_20 = (ymax - ymin)/New_N
    grad_20 = gradient_3point(new_p,res_20)
    wind_20 = geoWind(grad_20, physProps)
    error2 = abs(wind_20[10] - new_uExact[10])
    print('error2',error2)

    # calculate order of accuracy using errors at two different resolutions
    order_acc = (np.log(error1)-np.log(error2))/(np.log(res_10)-np.log(res_20))
    print('order of accuracy for 3 point',order_acc)
    
    # Create plot to estimate order of convergence
    plt.figure(2)
    ress = [res_10,res_20]
    errs = [error1,error2]
    plt.plot(ress,errs)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('resolution')
    plt.ylabel('error')
    plt.grid(True,which='both')
    plt.tight_layout()
    plt.savefig('Orderacc_3point.pdf')
    plt.show()
    
test_gradient_3point()
    